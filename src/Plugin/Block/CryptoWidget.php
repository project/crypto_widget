<?php

namespace Drupal\crypto_widget\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Crypto Widget Block.
 *
 * @Block(
 *   id = "crypto_widget",
 *   admin_label = @Translation("Crypto widget"),
 *   category = @Translation("Widget"),
 * )
 */
class CryptoWidget extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['crypto_widget'] = [
      '#type' => 'select',
      '#multiple' => FALSE,
      '#options' => [
        'BTC' => $this->t('Bitcoin'),
        'ETH' => $this->t('Ethereum'),
        'USDT' => $this->t('Tether'),
        'ADA' => $this->t('Cardano'),
        'BCH' => $this->t('Bitcoin Cash'),
        'LTC' => $this->t('Litecoin'),
        'UNI' => $this->t('Uniswap'),
        'LINK' => $this->t('Chainlink'),
        'ETC' => $this->t('Ethereum Classic'),
        'XLM' => $this->t('Stellar'),
        'USDC' => $this->t('USD Coin'),
        'FIL' => $this->t('Filecoin'),
        'WBTC' => $this->t('Wrapped Bitcoin'),
        'BSV' => $this->t('Bitcoin SV'),
        'ATOM' => $this->t('Cosmos'),
        'DASH' => $this->t('Dash'),
      ],
      '#title' => $this->t('Cryptocurrency'),
      '#description' => $this->t('The cryptocurrency to display the price.'),
      '#default_value' => $config['crypto_widget'] ?? '',
    ];

    $form['currency'] = [
      '#type' => 'select',
      '#multiple' => FALSE,
      '#options' => [
        'EUR' => 'EUR',
        'USD' => 'USD',
      ],
      '#title' => $this->t('Currency'),
      '#description' => $this->t('The currency in which to display the price.'),
      '#default_value' => $config['currency'] ?? '',
    ];

    $form['period'] = [
      '#type' => 'select',
      '#multiple' => FALSE,
      '#options' => [
        '5' => $this->t('5 minutes'),
        '10' => $this->t('10 minutes'),
        '30' => $this->t('30 minutes'),
        '60' => $this->t('60 minutes'),
      ],
      '#title' => $this->t('Period'),
      '#description' => $this->t('The period to refresh the price.'),
      '#default_value' => $config['period'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();

    $tokens = [
      'BTC' => 'Bitcoin',
      'ETH' => 'Ethereum',
      'USDT' => 'Tether',
      'ADA' => 'Cardano',
      'BCH' => 'Bitcoin Cash',
      'LTC' => 'Litecoin',
      'UNI' => 'Uniswap',
      'LINK' => 'Chainlink',
      'ETC' => 'Ethereum Classic',
      'XLM' => 'Stellar',
      'USDC' => 'USD Coin',
      'FIL' => 'Filecoin',
      'WBTC' => 'Wrapped Bitcoin',
      'BSV' => 'Bitcoin SV',
      'ATOM' => 'Cosmos',
      'DASH' => 'Dash',
    ];
    $token_label = $tokens[$values['crypto_widget']];
    $this->configuration['crypto_widget'] = $values['crypto_widget'];
    $this->configuration['currency'] = $values['currency'];
    $this->configuration['period'] = $values['period'];
    $this->configuration['options'] = $token_label;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    return [
      '#theme' => 'crypto_widget',
      '#ticket' => $config['crypto_widget'],
      '#token_label' => $config['options'],
      '#currency' => $config['currency'],
      '#period' => $config['period'],
      '#attached' => [
        'library' => 'crypto_widget/widget',
      ],
    ];
  }

}
