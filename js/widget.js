(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.cryptoWidget = {
    attach: function attach(context) {

      $(once('.block-crypto-widget', '.block-crypto-widget' )).each(function () {

        const ticket = $(this).find('.widget').attr('data-crypto-ticket');
        $(this).find('.crypto-widget-icon').addClass('icon-' + ticket.toLowerCase())
        const currency = $(this).find('.widget').attr('data-crypto-currency');
        const period = $(this).find('.widget').attr('data-crypto-period') * 60 * 1000;
        let lastPrice = 0;
        const that = $(this);

        //Get Date 1 day before
        var a = new Date();
        a.setDate(a.getDate() - 1);
        var date1 = new Date(a.getTime() - (a.getTimezoneOffset() * 60000)).toISOString().split("T")[0];

        //Get Date 1 week before
        var b = new Date();
        b.setDate(b.getDate() - 7);
        var date2 = new Date(b.getTime() - (b.getTimezoneOffset() * 60000)).toISOString().split("T")[0];

        //Define API calls URL
        const URL = 'https://api.coinbase.com/v2/prices/' + ticket + '-' + currency + '/spot';
        const URL1 = 'https://api.coinbase.com/v2/prices/' + ticket + '-' + currency + '/spot?date=' + date1;
        const URL2 = 'https://api.coinbase.com/v2/prices/' + ticket + '-' + currency + '/spot?date=' + date2;

        getData(URL, URL1, URL2, that);
        setInterval(function () { getData(URL, URL1, URL2, that); }, period);

        function getData(URL, URL1, URL2, that) {
          fetch(URL)
            .then(res => res.json())
            .then(function (data) {

              let today = new Date();
              let date = ('0' + today.getDate()).slice(-2) + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + today.getFullYear();
              let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
              let dateTime = date + ' ' + time;
              let price = data.data.amount;

              if (lastPrice === 0) {
                lastPrice = price;
              }
              //Get data from one day ago
              fetch(URL1)
                .then(res => res.json())
                .then(function (data) {
                  let variation = (lastPrice - data.data.amount) / (data.data.amount) * 100;
                  $(that).find('.crypto-widget-change-day').text(variation.toFixed(2) + '%');
                  variation > 0 ? $(that).find('.crypto-widget-change-day').addClass('value-positive') : $(that).find('.crypto-widget-change-day').removeClass('value-positive');
                })
                .catch(error => console.log(error))

              //Get data from one week ago
              fetch(URL2)
                .then(res => res.json())
                .then(function (data) {
                  let variation = (lastPrice - data.data.amount) / (data.data.amount) * 100;
                  $(that).find('.crypto-widget-change-week').text(variation.toFixed(2) + '%');
                  variation > 0 ? $(that).find('.crypto-widget-change-week').addClass('value-positive') : $(that).find('.crypto-widget-change-week').removeClass('value-positive');
                })
                .catch(error => console.log(error))

              let variation = (data.data.amount - lastPrice) / (lastPrice) * 100;
              $(that).find('.crypto-widget-price').text(parseFloat(price).toFixed(2));
              $(that).find('.crypto-widget-change').text(variation.toFixed(2) + '%');
              variation > 0 ? $(that).find('.crypto-widget-change').addClass('value-positive') : $(that).find('.crypto-widget-change').removeClass('value-positive');
              $(that).find('.crypto-widget-date').text(dateTime);
              lastPrice = price;
            })
            .catch(error => console.log(error))
        }
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
