CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Crypto Widget (crypto_widget) module provides a block to display cryptocurrencies price.
This modules makes use of Coinbase API to fetch the price for the interval configured in
the configuration form. Currently, the cryptocurrencies supported are the following:

 * Bitcoin
 * Ethereum
 * Tether
 * Cardano
 * Bitcoin Cash
 * Litecoin
 * Uniswap
 * Chainlink
 * Ethereum Classic
 * Stellar
 * USD Coin
 * Filecoin
 * Wrapped Bitcoin
 * Bitcoin SV
 * Cosmos
 * Dash

Icons are provided by http://cryptoicons.co/

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

   * Navigate to :

   Structure » Place Block » Crypto widget

   After choosing the region to place the block:

   - Select which cryptocurrency you would like to display the price;
   - The currency in which to fetch the price (euro or dollar are supported at this moment).
   - Select the period in which the widget will refresh the price
   - Click Save block.

MAINTAINERS
-----------

Current maintainers:
 * Nelson Alves (nsalves) - https://www.drupal.org/u/nsalves

This project has been sponsored by:
 * NTT DATA
   NTT DATA – a part of NTT Group – is a trusted global innovator of IT and business services headquartered in Tokyo.
   NTT is one of the largest IT services provider in the world and has 140,000 professionals, operating in more than 50 countries.
   NTT DATA supports clients in their digital development through a wide range of consulting and strategic advisory services, cutting-edge technologies, applications, infrastructure, modernization of IT and BPOs.
   We contribute with vast experience in all sectors of economic activity and have extensive knowledge of the locations in which we operate.
