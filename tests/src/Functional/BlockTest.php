<?php

namespace Drupal\Tests\crypto_widget\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests crypto widget blocks.
 *
 * @group crypto_widget
 */
class BlockTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['block', 'crypto_widget'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * A user with the 'administer blocks' permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Setup of the test.
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser(['administer blocks']);
    $this->drupalLogin($this->adminUser);
    $this->drupalPlaceBlock('crypto_widget', [
      'crypto_widget' => 'ETH',
      'options' => 'Ethereum',
      'currency' => 'EUR',
      'period' => '5',
    ],);
    $this->drupalLogout($this->adminUser);
  }

  /**
   * Tests that crypto widget block is visible in homepage.
   */
  public function testCryptoWidgetBlockVisibility() {
    // Array keyed list where key being the URL address and value being expected
    // visibility as boolean type.
    $paths = [
      '' => TRUE,
    ];
    foreach ($paths as $path => $expected_visibility) {
      $this->drupalGet($path);
      $elements = $this->xpath('//span[contains(@class,"crypto-widget-title")]');
      if ($expected_visibility) {
        $this->assertTrue(!empty($elements), 'Crypto Widget block in path "' . $path . '" should be visible');
      }
      else {
        $this->assertTrue(empty($elements), 'Crypto Widget block in path "' . $path . '" should not be visible');
      }
    }
  }

}
